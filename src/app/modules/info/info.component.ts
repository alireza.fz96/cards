import { Component } from '@angular/core';
import { Card } from './models/card.model';

@Component({
    selector: 'app-root',
    templateUrl: './info.component.html',
})
export class InfoComponent {
    cards: Card[] = [];

    onCardAdded(card: Card) {
        this.canBeAdded(card) && this.cards.push(card);
    }

    canBeAdded(newCard: Card) {
        return !this.cards.some((card) => card.name === newCard.name);
    }

    onCardRemoved(id: string) {
        this.cards = this.cards.filter((card) => card.id !== id);
    }
}
