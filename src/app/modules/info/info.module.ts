import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsComponent } from './components/cards/cards.component';
import { FormComponent } from './components/form/form.component';
import { InfoRoutingModule } from './info-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { InfoComponent } from './info.component';

@NgModule({
    declarations: [CardsComponent, FormComponent, InfoComponent],
    imports: [CommonModule, InfoRoutingModule, ReactiveFormsModule],
})
export class InfoModule {}
