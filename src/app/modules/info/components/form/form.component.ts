import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Card } from '../../models/card.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'app-form',
    templateUrl: './form.component.html',
    styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
    userForm: FormGroup;

    @Output() cardAdded = new EventEmitter<Card>();

    constructor() {}

    ngOnInit() {
        this.userForm = new FormGroup({
            name: new FormControl('', Validators.required),
            age: new FormControl('', Validators.required),
        });
    }

    addCard() {
        const { name, age } = this.userForm.value;
        this.cardAdded.emit(new Card(name, age, Date.now().toString()));
        this.userForm.reset();
    }
}
