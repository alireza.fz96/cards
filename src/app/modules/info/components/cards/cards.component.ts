import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Card } from '../../models/card.model';

@Component({
    selector: 'app-cards',
    templateUrl: './cards.component.html',
    styleUrls: ['./cards.component.scss'],
})
export class CardsComponent implements OnInit {
    @Input() cards: Card[];
    @Output() cardRemoved = new EventEmitter<string>();

    constructor() {}

    ngOnInit(): void {}

    removeCard(id: string) {
        this.cardRemoved.emit(id);
    }
}
